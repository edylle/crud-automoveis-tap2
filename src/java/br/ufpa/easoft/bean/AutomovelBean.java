package br.ufpa.easoft.bean;

import br.ufpa.easoft.rn.AutomovelRN;
import br.ufpa.easoft.rn.interfaces.AutomovelRNInterface;
import br.ufpa.easoft.vo.AutomovelVO;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "automovelBean")
@ViewScoped
public class AutomovelBean {

      private AutomovelVO auto;
      private List<AutomovelVO> listaAutos;
      private final AutomovelRNInterface autoRN;

      public AutomovelBean() {
            auto = new AutomovelVO();
            listaAutos = new LinkedList<>();
            autoRN = new AutomovelRN();
      }

      public void salvar() {
            autoRN.salvar(auto);
            
      }

      public AutomovelVO getAuto() {
            return auto;
      }

      public void setAuto(AutomovelVO auto) {
            this.auto = auto;
      }

      public List<AutomovelVO> getListaAutos() {
            return listaAutos;
      }

      public void setListaAutos(List<AutomovelVO> listaAutos) {
            this.listaAutos = listaAutos;
      }

}
