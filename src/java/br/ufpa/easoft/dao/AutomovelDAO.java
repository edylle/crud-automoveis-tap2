package br.ufpa.easoft.dao;

import br.ufpa.easoft.vo.AutomovelVO;
import br.ufpa.easoft.vo.MarcaVO;
import br.ufpa.easoft.vo.ModeloVO;
import java.util.List;
import javax.persistence.TypedQuery;

public class AutomovelDAO extends GenericDAO<AutomovelVO> {

      public List<AutomovelVO> buscarPorMarca(MarcaVO marca) {
            TypedQuery<AutomovelVO> tq = getEntityManager().createQuery("SELECT a FROM AutomovelVO a "
                    + "WHERE a.modelo.marca.id = :id", AutomovelVO.class);
            tq.setParameter("id", marca.getId());
            return tq.getResultList();
      }

      public List<AutomovelVO> buscarPorModelo(ModeloVO modelo) {
            TypedQuery<AutomovelVO> tq = getEntityManager().createQuery("SELECT a FROM AutomovelVO a "
                    + "WHERE a.modelo.id = :id", AutomovelVO.class);
            tq.setParameter("id", modelo.getId());
            return tq.getResultList();
      }

      public List<AutomovelVO> buscarPorCaracteristicaCompleta(Integer anoFab, Integer anoMod, Float preco, Integer km) {
            TypedQuery<AutomovelVO> tq = getEntityManager().createQuery("SELECT a FROM AutomovelVO a "
                    + "WHERE a.anoFabricacao = :ano AND a.anoModelo = :mod AND a.preco = :preco "
                    + "AND a.kilometragem = :km", AutomovelVO.class);
            tq.setParameter("ano", anoFab);
            tq.setParameter("mod", anoMod);
            tq.setParameter("preco", preco);
            tq.setParameter("km", km);
            return tq.getResultList();
      }
      
      public List<AutomovelVO> buscarPorCaracteristicaParcial(Integer anoFab, Integer anoMod, Float preco, Integer km) {
            TypedQuery<AutomovelVO> tq = getEntityManager().createQuery("SELECT a FROM AutomovelVO a "
                    + "WHERE a.anoFabricacao = :ano OR a.anoModelo = :mod OR a.preco = :preco "
                    + "OR a.kilometragem = :km", AutomovelVO.class);
            tq.setParameter("ano", anoFab);
            tq.setParameter("mod", anoMod);
            tq.setParameter("preco", preco);
            tq.setParameter("km", km);
            return tq.getResultList();
      }

}
