package br.ufpa.easoft.rn;

import br.ufpa.easoft.dao.AutomovelDAO;
import br.ufpa.easoft.rn.interfaces.AutomovelRNInterface;
import br.ufpa.easoft.vo.AutomovelVO;
import br.ufpa.easoft.vo.MarcaVO;
import br.ufpa.easoft.vo.ModeloVO;
import java.util.List;

public class AutomovelRN implements AutomovelRNInterface {

      private final AutomovelDAO dao;

      public AutomovelRN() {
            dao = new AutomovelDAO();
      }

      @Override
      public void salvar(AutomovelVO auto) {
            dao.startOperation();
            dao.save(auto);
            dao.stopOperation(true);
      }

      @Override
      public void remover(AutomovelVO auto) {
            dao.startOperation();
            dao.delete(auto);
            dao.stopOperation(true);
      }

      @Override
      public List<AutomovelVO> listar() {
            dao.startOperation();
            List<AutomovelVO> autos = dao.findAll(AutomovelVO.class);
            dao.stopOperation(false);
            return autos;
      }

      @Override
      public void atualizar(AutomovelVO auto) {
            dao.startOperation();
            dao.update(auto);
            dao.stopOperation(true);
      }

      @Override
      public List<AutomovelVO> listarPorMarca(MarcaVO marca) {
            dao.startOperation();
            List<AutomovelVO> autos = dao.buscarPorMarca(marca);
            dao.stopOperation(false);
            return autos;
      }

      @Override
      public List<AutomovelVO> listarPorModelo(ModeloVO modelo) {
            dao.startOperation();
            List<AutomovelVO> autos = dao.buscarPorModelo(modelo);
            dao.stopOperation(false);
            return autos;
      }

      @Override
      public List<AutomovelVO> listarPorCaracteristicaCompleta(Integer anoFab, Integer anoMod, Float preco, Integer km) {
            dao.startOperation();
            List<AutomovelVO> autos = dao.buscarPorCaracteristicaCompleta(anoFab, anoMod, preco, km);
            dao.stopOperation(false);
            return autos;
      }

      @Override
      public List<AutomovelVO> listarPorCaracteristicaParcial(Integer anoFab, Integer anoMod, Float preco, Integer km) {
            dao.startOperation();
            List<AutomovelVO> autos = dao.buscarPorCaracteristicaParcial(anoFab, anoMod, preco, km);
            dao.stopOperation(false);
            return autos;
      }

}
