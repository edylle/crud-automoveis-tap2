package br.ufpa.easoft.rn.interfaces;

import br.ufpa.easoft.vo.AutomovelVO;
import br.ufpa.easoft.vo.MarcaVO;
import br.ufpa.easoft.vo.ModeloVO;
import java.util.List;

/**
 *
 * @author Weslley Tavares <weslleysammyr@gmail.com>
 */
public interface AutomovelRNInterface {

      void salvar(AutomovelVO auto);

      void remover(AutomovelVO auto);

      List<AutomovelVO> listar();

      void atualizar(AutomovelVO auto);

      List<AutomovelVO> listarPorMarca(MarcaVO marca);

      List<AutomovelVO> listarPorModelo(ModeloVO marca);

      List<AutomovelVO> listarPorCaracteristicaCompleta(Integer anoFab, Integer anoMod, Float preco, Integer km);

      List<AutomovelVO> listarPorCaracteristicaParcial(Integer anoFab, Integer anoMod, Float preco, Integer km);

}
