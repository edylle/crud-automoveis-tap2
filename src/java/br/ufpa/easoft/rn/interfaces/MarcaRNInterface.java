package br.ufpa.easoft.rn.interfaces;

import br.ufpa.easoft.vo.MarcaVO;
import java.util.List;

/**
 *
 * @author Weslley Tavares <weslleysammyr@gmail.com>
 */
public interface MarcaRNInterface {

      void salvar(MarcaVO auto);

      void remover(MarcaVO auto);

      List<MarcaVO> listar();

      void atualizar(MarcaVO auto);

}
