/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufpa.easoft.rn.interfaces;

import br.ufpa.easoft.vo.ModeloVO;
import java.util.List;

/**
 *
 * @author Weslley Tavares <weslleysammyr@gmail.com>
 */
public interface ModeloRNInterface {

      void salvar(ModeloVO auto);

      void remover(ModeloVO auto);

      List<ModeloVO> listar();

      void atualizar(ModeloVO auto);

}
