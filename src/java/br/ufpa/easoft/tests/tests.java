package br.ufpa.easoft.tests;

import br.ufpa.easoft.dao.AutomovelDAO;
import br.ufpa.easoft.vo.AutomovelVO;

public class tests {

    public static void main(String[] args) {
        AutomovelDAO dao = new AutomovelDAO();
        AutomovelVO vo = new AutomovelVO();
        
        vo.setAnoFabricacao(2013);
        vo.setAnoModelo(2014);
        vo.setKilometragem(12000);
        vo.setObservacoes("Carro semi-novo");
        vo.setPreco(32.000f);
        
        dao.startOperation();
        dao.save(vo);
        dao.stopOperation(true);
    }
    
}
