package br.ufpa.easoft.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "automovel")
public class AutomovelVO implements Serializable {

      private static final long serialVersionUID = 1L;

      @Id
      @GeneratedValue(strategy = GenerationType.AUTO)
      private Integer id;

      @Column(name = "ano_fabricacao")
      private Integer anoFabricacao;

      @Column(name = "ano_modelo")
      private Integer anoModelo;

      private String observacoes;

      private Float preco;

      private Integer kilometragem;

      @ManyToOne
      private ModeloVO modelo;

      public Integer getId() {
            return id;
      }

      // GETTERS AND SETTERS
      public Integer getAnoFabricacao() {
            return anoFabricacao;
      }

      public void setAnoFabricacao(Integer anoFabricacao) {
            this.anoFabricacao = anoFabricacao;
      }

      public Integer getAnoModelo() {
            return anoModelo;
      }

      public void setAnoModelo(Integer anoModelo) {
            this.anoModelo = anoModelo;
      }

      public String getObservacoes() {
            return observacoes;
      }

      public void setObservacoes(String observacoes) {
            this.observacoes = observacoes;
      }

      public Float getPreco() {
            return preco;
      }

      public void setPreco(Float preco) {
            this.preco = preco;
      }

      public Integer getKilometragem() {
            return kilometragem;
      }

      public void setKilometragem(Integer kilometragem) {
            this.kilometragem = kilometragem;
      }

      public ModeloVO getModelo() {
            return modelo;
      }

      public void setModelo(ModeloVO modelo) {
            this.modelo = modelo;
      }

      @Override
      public int hashCode() {
            int hash = 0;
            hash += (id != null ? id.hashCode() : 0);
            return hash;
      }

      @Override
      public boolean equals(Object object) {
            // TODO: Warning - this method won't work in the case the id fields are not set
            if (!(object instanceof AutomovelVO)) {
                  return false;
            }
            AutomovelVO other = (AutomovelVO) object;
            if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
                  return false;
            }
            return true;
      }

      @Override
      public String toString() {
            return "br.ufpa.easoft.vo.AutomovelVO[ id=" + id + " ]";
      }

}
