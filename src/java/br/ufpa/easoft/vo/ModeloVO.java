package br.ufpa.easoft.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "modelo")
public class ModeloVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String descricao;

    private Integer potencia;

    @OneToMany(mappedBy = "modelo")
    private List<AutomovelVO> automoveis = new ArrayList<>();

    @ManyToOne
    private MarcaVO marca;

    // GETTERS AND SETTERS
    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getPotencia() {
        return potencia;
    }

    public void setPotencia(Integer potencia) {
        this.potencia = potencia;
    }

    public List<AutomovelVO> getAutomoveis() {
        return automoveis;
    }

    public void setAutomoveis(List<AutomovelVO> automoveis) {
        this.automoveis = automoveis;
    }

    public MarcaVO getMarca() {
        return marca;
    }

    public void setMarca(MarcaVO marca) {
        this.marca = marca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeloVO)) {
            return false;
        }
        ModeloVO other = (ModeloVO) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufpa.easoft.vo.ModeloVO[ id=" + id + " ]";
    }

}
